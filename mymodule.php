<?php
/***************************************
 *
 * MyModule
 * Special class for usual module task
 *
 * Author : Eftar
 *
 * Licence publique générale GNU 3.0
 * (voir fichier license.md)
 *
 ***************************************/

if (!defined('_PS_VERSION_'))
	exit;

class Mymodule extends Module
{
	const INSTALL_SQL_FILE 		= 'install.sql';
	const UNINSTALL_SQL_FILE 	= 'uninstall.sql';

	protected $rConfig 		= array();
	protected $rHook 		= array();
	protected $rAdminCtrl 	= array();
	protected $rOverride 	= array();

	protected $debug 		= false;
	protected $debugSQL 	= false;

	public function __construct()
	{

		parent::__construct();

		$this->need_instance 	= 1;
		$this->bootstrap 		= true;

		$this->url = __PS_BASE_URI__ . basename(_PS_MODULE_DIR_) . '/' . $this->name . '/';

		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

	}

	public function install()
	{
        $this->log('parent::install Call');
		if ( ! parent::install())
			return false;
        $this->log('parent::install OK');

        $this->log('registerHook Start');
		foreach($this->rHook as $hook)
		{
            $this->log('registerHook '.$hook);
			if ( ! $this->registerHook($hook))
				return false;
            $this->log('Hook '.$hook.' registred');
		}

        $this->log('realRgisterHook Start');
        $this->realRegisterHooks();

		$sql = '';
		// SQL Stuff create /modify tables
		if (file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            $this->log('Loading SQL file');
			$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE);
			if ($sql === false)
				return false;
            $this->log('SQL file loaded');
		}
		$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
		if ($sql != '')
			$sql = preg_split("/;\s*[\r\n]+/", trim($sql));
		else
			$sql = array();
		foreach ($sql as $query)
		{
            $this->log('SQL Query Call');
			if ( ! Db::getInstance()->execute(trim($query)))
			{
				echo "Error with sql : <pre>\n".$query."</pre><br/>\n";
                $this->log('Error with sql : '.$query);
				//TODO Add specific token like /*compulsory*/ to throw an error if it fail
			}
		}

        $this->log('Install Admin Ctrl');
		//Install admin controller
		foreach($this->rAdminCtrl as $adminCtrl)
		{
			$this->installModuleTab($adminCtrl['ctrl'], $adminCtrl['name'], $adminCtrl['parent']);
		}

        $this->log('Install Override');
        //Install Override
	    if (isset($this->rOverride) && is_array($this->rOverride))
	    {
			foreach($this->rOverride as $path)
				$this->installOverride($path);
		}

		return true;
	}

	public function uninstall()
	{

        $this->log('parent::uninstall Call');
		if ( ! parent::uninstall())
			return false;

        $this->log('parent::uninstall OK');

        $this->log('unRegisterHook Start');
		foreach($this->rHook as $hook)
		{
            $this->log('unRegisterHook '.$hook);
			$this->unRegisterHook($hook);
		}

		$sql = '';
		// SQL Stuff undo
		// SQL Stuff create /modify tables
		if (file_exists(dirname(__FILE__).'/'.self::UNINSTALL_SQL_FILE)) {

            $this->log('Loading SQL file');
			$sql = file_get_contents(dirname(__FILE__).'/'.self::UNINSTALL_SQL_FILE);
			if ($sql === false)
				return false;
            $this->log('SQL file loaded');
		}

		$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
		if ($sql != '')
			$sql = preg_split("/;\s*[\r\n]+/", trim($sql));
		else
			$sql = array();

		foreach ($sql as $query)
		{
            $this->log('SQL Query Call');
			if ( ! Db::getInstance()->execute(trim($query)))
			{
				echo "Error with sql : <pre>\n".$query."</pre><br/>\n";
                $this->log('Error with sql : '.$query);

				//TODO Add specific token like /*compulsory*/ to throw an error if it fail
				//return false;
			}
		}

        $this->log('Uninstall Admin Ctrl');

		//Uninstall admin controller
		foreach($this->rAdminCtrl as $adminCtrl)
		{
			$this->uninstallModuleTab($adminCtrl['ctrl']);
		}

		return true;
	}

	/**
	 * Méthode Standard
	 * Traitement du formulaire de configuration
	 */
	public function getContent()
	{

		$output = null;

		if (Tools::isSubmit('submit'.$this->name))
		{
			foreach($this->rConfig as $aConfig)
			{
				$aConfVal = strval(Tools::getValue($aConfig));
				Configuration::updateValue($aConfig, $aConfVal);
			}

			$output .= $this->displayConfirmation($this->l('Settings updated'));
		}

		return $output.$this->displayFormConfig();

	}

	/**
	 * Méthode Standard
	 * Affichage du formulaire de configuration
	 */
	public function displayFormConfig($rInput = array())
	{

		// Get default Language
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		// Init Fields form array
		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
			),
			'input' => $rInput,
			'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'button'
			)
		);

		$helper = new HelperForm();

		// Module, token and currentIndex
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

		// Language
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;

		// Title and toolbar
		$helper->title = $this->displayName;
		$helper->show_toolbar 	= true;		// false -> remove toolbar
		$helper->toolbar_scroll = true;		// yes - > Toolbar is always visible on the top of the screen.
		$helper->submit_action 	= 'submit'.$this->name;
		$helper->toolbar_btn 	= array(
			'save' =>
			array(
					'desc' => $this->l('Save'),
					'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
					'&token='.Tools::getAdminTokenLite('AdminModules'),
			),
			'back' => array(
					'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
					'desc' => $this->l('Back to list')
			)
		);

		// Load current value
		foreach($this->rConfig as $aConfig)
		{
			$helper->fields_value[ $aConfig ] = Configuration::get($aConfig);
		}
		return $helper->generateForm($fields_form);

	}

	protected function installModuleTab($class = null, $name = null, $idTabParent = 0)
	{
		$pass = true;

		//already installed ?
		if ( Tab::getIdFromClassName($class) )
			return true;

		if ( ! is_int($idTabParent) && $idTabParent !== '-1')
			$idTabParent = Tab::getIdFromClassName($idTabParent);

		if ( ! is_int($idTabParent) && $idTabParent !== '-1')
			return false;


		$tab = new Tab();
		foreach (Language::getLanguages(true) as $lang) {
		    $tab->name[ $lang['id_lang'] ] = $name;
		}
		$tab->class_name = $class;
		$tab->module = $this->name;
		$tab->id_parent = (int) $idTabParent;

		$pass = $tab->save();

		return $pass;
	}

	private function uninstallModuleTab($class)
	{
		$pass = true;
		$idTab = Tab::getIdFromClassName($class);
		if ($idTab != 0)
		{
			$tab = new Tab($idTab);
			$pass = $tab->delete();
		}
		return($pass);
	}

    /**
	 * Register hooks
	 */
	public function realRegisterHooks()
	{
	 	if (isset($this->hooks) && is_array($this->hooks))
	    {
			foreach($this->hooks as $hook)
			{
				if (!$this->isRegisteredInHook($hook))
				{
					if (!$this->registerHook($hook))
					{
						echo "Error installing Hooks '".$hook."'<br/>\n";
						return false;
					}
				}
			}
		}
	}

    function installOverride($path)
	{
		$override_src = $this->getLocalPath().'override'.DIRECTORY_SEPARATOR.$path;
		$override_dest = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override'.DIRECTORY_SEPARATOR.$path;
		@mkdir(dirname($override_dest), 0705, true);
		if (!is_writable(dirname($override_dest)))
			throw new Exception(sprintf(Tools::displayError('directory (%s) not writable'), dirname($override_dest)));
		copy($override_src, $override_dest);
	}

	function uninstallOverride($path)
	{
		if (is_file(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$path) && !unlink(_PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'override'.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$path))
			throw new Exception(sprintf(Tools::displayError('Error deleting template: (%s)'), $path));
	}

	/**
	 * Function to log error in module directory only on debug mode
	 **/
	public function log($string)
	{
        if ($this->debug)
            file_put_contents(dirname(__FILE__).'/log.txt', date('Ymd-His').':'.$string."\r\n", FILE_APPEND);
	}

}