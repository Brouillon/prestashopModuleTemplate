<?php

$rModuleTab = array(
	array('administration', 'Administration'),
    array('advertising_marketing', 'Advertising & Marketing'),
    array('analytics_stats', 'Analytics & Stats'),
    array('billing_invoicing', 'Billing & Invoices'),
    array('checkout', 'Checkout'),
    array('content_management', 'Content Management'),
    array('dashboard', 'Dashboard'),
    array('emailing', 'E-mailing'),
    array('export', 'Export'),
    array('front_office_features', 'Front Office Features'),
    array('i18n_localization', 'I18n & Localization'),
    array('market_place', 'Market Place'),
    array('merchandizing', 'Merchandizing'),
    array('migration_tools', 'Migration Tools'),
    array('mobile', 'Mobile'),
    array('others', 'Other Modules'),
    array('payments_gateways', 'Payments & Gateways'),
    array('payment_security', 'Payment Security'),
    array('pricing_promotion', 'Pricing & Promotion'),
    array('quick_bulk_update', 'Quick / Bulk update'),
    array('search_filter', 'Search & Filter'),
    array('seo', 'SEO'),
    array('shipping_logistics', 'Shipping & Logistics'),
    array('slideshows', 'Slideshows'),
    array('smart_shopping', 'Smart Shopping'),
    array('social_networks', 'Social Networks')
);

$rAdminTab = array(
	'AdminDashboard',
	'AdminCatalog',
	'AdminParentOrders',
	'AdminParentCustomer',
	'AdminPriceRule',
	'AdminParentModules',
	'AdminParentShipping',
	'AdminParentLocalization',
	'AdminParentPreferences',
	'AdminTools',
	'AdminAdmin',
	'AdminParentStats',
	'AdminStock'
);

//ToDo Read config.json file for predefined variable


$name = false;
while ($name === false)
{
	$name = readline("Name of the module ?".PHP_EOL);
	if ( ! preg_match('#[a-zA-Z0-9]*#', $name))
	{
		echo "# Incorrect name (only a-zA-Z0-9 chars are allowed)".PHP_EOL;
		$tab = false;
	}
}

$author 		= readline("Author of the module ? ".PHP_EOL);
$displayName 	= readline("DisplayName of the module ? ".PHP_EOL);
$description 	= readline("Description of the module ? ".PHP_EOL);

$str = '';
$i = 0;
foreach ($rModuleTab as $tab)
{
	$str .= $i." ".$tab[1].PHP_EOL;
	$i++;
}
$tab = false;
while ($tab === false)
{
	$tab = readline("Tab of the module : ".PHP_EOL.$str);
	if (isset($rModuleTab[ $tab ]))
		$tab = $rModuleTab[ $tab ][0];
	else {
		echo "# Incorrect value".PHP_EOL;
		$tab = false;
	}
}
$useAdminController = readline("Use adminCtrl (y or n) ? ".PHP_EOL);
if ($useAdminController == 'y')
{
	$adminCtrlName = false;
    while ($adminCtrlName === false)
    {
        $adminCtrlName = readline("Name of the adminCtrl ?".PHP_EOL);
    }

    $str = '';
	$i = 0;
	foreach ($rAdminTab as $tab)
	{
		$str .= $i." ".$tab.PHP_EOL;
		$i++;
	}
	$adminTab = false;
	while ($adminTab === false)
	{
		$adminTab = readline("Tab for the adminCtrl (-1 no parent): ".PHP_EOL.$str);
		if (isset($rAdminTab[ $adminTab ]))
			$tab = $rAdminTab[ $adminTab ];
		elseif ($adminTab === '-1')
			$tab = -1;
		else {
			echo "# Incorrect value".PHP_EOL;
			$adminTab = false;
		}
	}

	$rAdminCtrl = 'array(array(\'ctrl\' => \'Admin'.$name.'\', \'name\' => \''.$adminCtrlName.'\', \'parent\' => \''.$tab.'\'))';

}
else
	$rAdminCtrl = 'array()';

$useFrontController = readline("Use frontCtrl (y or n) ? ".PHP_EOL);

if ($useFrontController == 'y')
{
	$frontCtrl = '$this->controllers = array(\'front'.$name.'\');';
}
else
{
    $frontCtrl = '';
}

//Write module files
$moduleFile = '<?php
/***************************************
 *
 * '.$name.'
 * '.$description.'
 *
 * Author : '.$author.'
 *
 ***************************************/

require_once \'mymodule.php\';

if (!defined(\'_PS_VERSION_\'))
	exit;

class '.$name.' extends MyModule
{

	protected $rConfig 	= array();
	protected $rHook 		= array();
	protected $rAdminCtrl = '.$rAdminCtrl.';

	protected $debug 		= false;
	protected $debugSQL 	= false;

	public function __construct()
	{

		$this->name 	= \''.$name.'\';
		$this->tab 		= \''.$tab.'\';
		$this->version 	= \'1.0\';
		$this->author 	= \''.$author.'\';

		$this->displayName = $this->l(\''.addslashes($displayName).'\');
		$this->description = $this->l(\''.addslashes($description).'\');
		$this->ps_versions_compliancy = array(\'min\' => \'1.6\', \'max\' => _PS_VERSION_);

		'.$frontCtrl.'

		parent::__construct();

	}

}';
file_put_contents(dirname(__FILE__).'/'.$name.'.php', $moduleFile);

//Write admin Ctrl file
if ($useAdminController == 'y')
{
	$adminCtrlFile = '<?php

class Admin'.$name.'Controller extends ModuleAdminController
{
	public $module	= \''.$name.'\' ;
	public $name 	= \''.$name.'\' ;

	public function __construct()
	{
		$this->path = _PS_MODULE_DIR_ . $this->module . \'/\' ;

		$this->className = \''.$name.'\';

		$this->display = Tools::getValue(\'display\');

		$this->lang = true;
		$this->deleted = false;
		$this->colorOnBackground = false;

		$this->url = __PS_BASE_URI__ . basename(_PS_MODULE_DIR_) . \'/\' . $this->name . \'/\' ;

		$this->context = Context::getContext();

		$this->admintoken = Tools::getAdminTokenLite(\'AdminModules\');

		parent::__construct();

	}


	/**
	 * Init content to catch ajax call
	 */
	public function initContent()
	{
		parent::initContent();
	}

}';
	file_put_contents(dirname(__FILE__).'/controllers/admin/Admin'.$name.'.php', $adminCtrlFile);

}

//Write admin Ctrl file
if ($useFrontController == 'y')
{
	$frontCtrlFile = '<?php

class '.$name.'front'.strtolower($name).'ModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
	}

}';
	file_put_contents(dirname(__FILE__).'/controllers/front/Front'.$name.'.php', $frontCtrlFile);

}


// adminCtrml / frontCtrl

//preg_replace('', '');