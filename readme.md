# Prestashop Module Template

Permet de générer un squelette pour le développement d'un module prestashop 1.6

## Prérequis
Nécessite php-readline

## Utilisation :

Dans le dossier :
```
php configure.php
```

Vous sera alors demandé :

- Le nom du module
- L'auteur
- Le nom à afficher
- La description
- La catégorie du module
- L'utilisation d'un Controller Admin, le cas échéant le parent pour le rattachement
- L'utilisation d'un Controller Front

En fonction de vos choix les fichiers seront créés
Vous devez ensuite renommer le dossier du module en utilisant le nom en minuscule que vous avez choisi précédemment

/!\ Veuillez pour l'instant vous contenter de caractères ascii lors de la saisie de ces éléments

**That's it !**

### Configuration standard du module

Possibilité de personnaliser dans votre module la page de configuration standard

- Créer la fonction ```public function displayFormConfig()```
- Dans la définition, appeler la fonction parente ```parent::displayFormConfig($rOption);```
- Exemple de format de l'argument attendu par cette fonction :

```
array(
	array(
		'type' => 'select',
		'options' => array(
			'query' => Carrier::getCarriers(1),
			'id' => 'id_carrier',
			'name' => 'name'
		),
		'label' => $this->l('Transporteur pour les commandes importées'),
		'name' => 'IMPORTORDER_CARRIER_ID',
		'required' => true,
	),
	array(
		'type' => 'select',
		'options' => array(
			'query' 	=> array(
				array('id' => '0',	'name' => $this->l('Création panier unique')),
				array('id' => '1',	'name' => $this->l('Création commande unique')),
				array('id' => '2',	'name' => $this->l('Création panier puis séparation par Adresse')),
				array('id' => '3',	'name' => $this->l('Création commande puis séparation par Adresse'))
			),
			'id' 		=> 'id',
			'name' 		=> 'name'
		),
		'label' => $this->l('Par défaut lors de l\'import'),
		'name' => 'IMPORTORDER_CREATE_ORDER',
		'required' => true,
	),
	array(
		'type' => 'select',
		'options' => array(
			'query' => OrderState::getOrderStates(1),
			'id' => 'id_order_state',
			'name' => 'name'
		),
		'label' => $this->l('Statut par défaut des commandes importées'),
		'name' => 'IMPORTORDER_ORDER_STATE_ID',
		'required' => true,
	)
)
```

- Renseigner les noms des paramètres de configuration dans la variable membre de votre classe ```$rConfig``` 

Si vous suivez cette procédure les valeurs seront automatiquement ajoutées et enregistrées dans le formulaire \o/